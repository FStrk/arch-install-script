#!/bin/sh

# Configure AUR and install yay
# Requires: git

# Exit if any command fails
set -e

# Configure 
mkdir -pv $HOME/.cache/yay
cd $HOME/.cache/yay

# Install yay
echo "Installing yay..."
git clone https://aur.archlinux.org/yay.git
cd yay
makepkg -si

cd $HOME
echo ""
echo "Finished!"
