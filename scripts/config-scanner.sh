#!/bin/sh

# This script configures the scanner DCP-585CW and MFC-J4420DW from Brother.
# This script needs yay.

# Install scanner-specific packages
echo ""
echo "Installing scanner-specific packages..."
yay -S brscan3 brscan4

# Configure scanner
echo ""
echo "Configuring scanner..."
sudo brsaneconfig3 -a name=DCP-585CW model=DCP-585CW nodename=BRWC417FE160A8E
sudo brsaneconfig4 -a name=MFC-J4420DW model=MFC-J4420DW nodename=BRW3468951D660C

echo ""
echo "Finished!"
echo "Try a scan..."
sudo -H simple-scan
