#!/bin/sh

################################################################################
# Configurable variables                                                       #
################################################################################

# Config directory
CONFIG_DIR="$HOME/.config"

# Directory of your SSH keys
SSH_KEYS_DIR=""

# Keepass key file
KEEPASS_KEY_FILE=""

# Kubectl config file
KUBECTL_FILE=""

# Wireguard config file
WG_CONFIG_FILE=""



################################################################################
# System programs                                                              #
################################################################################

PROGRAMS_SYSTEM=""

# Core
PROGRAMS_SYSTEM+="glib2 "

# System info
PROGRAMS_SYSTEM+="neofetch "
PROGRAMS_SYSTEM+="htop gnome-system-monitor "
PROGRAMS_SYSTEM+="tree "

# Documentation
PROGRAMS_SYSTEM+="man-db man-pages texinfo "

# Shell
PROGRAMS_SYSTEM+="zsh-syntax-highlighting zsh-completions zsh-autosuggestions bash-completion jq "

# Download
PROGRAMS_SYSTEM+="curl wget "

# Archive support
PROGRAMS_SYSTEM+="zip unzip tar xarchiver p7zip "

# Fonts
PROGRAMS_SYSTEM+="ttf-dejavu ttf-droid ttf-hack ttf-font-awesome otf-font-awesome ttf-lato ttf-liberation libertinus-font ttf-opensans ttf-roboto ttf-inconsolata terminus-font ttf-ubuntu-font-family noto-fonts noto-fonts-emoji "

# File system support
PROGRAMS_SYSTEM+="btrfs-progs dosfstools e2fsprogs ntfs-3g "
# Android
PROGRAMS_SYSTEM+="gvfs-mtp mtpfs "

# Network tools
PROGRAMS_SYSTEM+="iw wpa_supplicant crda "

# Power management
PROGRAMS_SYSTEM+="acpi "

# Logging
PROGRAMS_SYSTEM+="logrotate "


################################################################################
# Desktop environment                                                          #
################################################################################

PROGRAMS_DE=""
PROGRAMS_DE_AUR=""


# User directories
PROGRAMS_DE+="xdg-user-dirs "

# Display driver
PROGRAMS_DE+="xf86-video-intel libgl mesa vulkan-intel "

# Display server protocol
PROGRAMS_DE+="wayland xorg-xwayland qt5ct qt5-wayland qt6-wayland "

# Window manager
PROGRAMS_DE+="sway "

# Status bar
PROGRAMS_DE+= "waybar libappindicator-gtk3 "
PROGRAMS_SYSTEM+="network-manager-applet "

# Idle
PROGRAMS_DE_AUR+="swayidle swaylock-effects "

# Launcher
PROGRAMS_DE+="bemenu "
PROGRAMS_DE_AUR="j4-dmenu-desktop "

# Monitor config
PROGRAMS_DE+="kanshi "

# Multimedia framework
PROGRAMS_DE+="pipewire wireplumber "

# Audio
PROGRAMS_DE+="pipewire-pulse pipewire-alsa pipewire-jack pavucontrol helvum "

# Bluetooth
PROGRAMS+="bluez bluez-utils blueman "

# Screensharing
PROGRAMS_DE+="xdg-desktop-portal xdg-desktop-portal-wlr "

# Notifications
PROGRAMS_DE+="libnotify dunst "

# Screenshot
PROGRAMS_DE+="grim slurp "
PROGRAMS_DE_AUR+="grimshot "

# Clipboard
PROGRAMS_DE+="wl-clipboard "

# Backlight
PROGRAMS_DE+="light "

# Screen temperature
PROGRAMS_DE+="gammastep "

# Playerctl
PROGRAMS_DE+="playerctl "

# Theme
# PROGRAMS_DE+="lxappearance "
PROGRAMS_DE+="gnome-themes-extra materia-gtk-theme breeze-icons "

# Debugging
PROGRAMS_DE_AUR+="wev xorg-xprop "


################################################################################
# Desktop programs                                                             #
################################################################################

PROGRAMS=""
PROGRAMS_AUR=""

# Terminal
PROGRAMS+="alacritty "

# File roller
PROGRAMS+="nemo "
PROGRAMS+="nemo-seahorse nemo-audio-tab nemo-fileroller nemo-image-converter nemo-preview nemo-python "
PROGRAMS+="ffmpegthumbnailer "

# Browser
PROGRAMS+="firefox chromium "

# Mail client
PROGRAMS+="thunderbird "

# Editors
PROGRAMS+="gedit code "

# Office
PROGRAMS+="libreoffice-still hunspell hunspell-de hyphen hyphen-de libmythes mythes-de "
PROGRAMS+="evince pdfarranger "
PROGRAMS+="scribus "
PROGRAMS+="xournalpp "
PROGRAMS+="texlive-most texstudio biber "

# Image
PROGRAMS+="eog "
PROGRAMS+="gimp inkscape "
PROGRAMS+="imagemagick img2pdf "

# Audio and video
PROGRAMS+="vlc "
PROGRAMS_AUR+="spotify "

# Webcam
PROGRAMS+="cheese "

# Sound recorder
PROGRAMS+="gnome-sound-recorder "

# Development
PROGRAMS+="python python-numpy python-scipy python-matplotlib python-vobject python-pyqt5 python-pyqt6 python-opencv python2 "
PROGRAMS+="jre-openjdk jdk-openjdk openjdk-doc "
PROGRAMS+="docker "
PROGRAMS_AUR+="fgallery "
PROGRAMS+="kubectl helm "

# Cloud
PROGRAMS+="nextcloud-client "

# Keys
PROGRAMS+="gnome-keyring libsecret seahorse polkit-gnome "
PROGRAMS+="keepassxc "

# Communication
PROGRAMS+="mumble "
PROGRAMS+="discord "
PROGRAMS+="signal-desktop "
PROGRAMS_AUR+="zoom "

# Printer
PROGRAMS+="cups cups-pdf gutenprint system-config-printer "

# Scanner
PROGRAMS+="sane sane-airscan ipp-usb simple-scan "

# Calculator
PROGRAMS+="galculator "

# OBS
PROGRAMS+="obs-studio libva-intel-driver "

# Virtual cam
PROGRAMS+="v4l2loopback-dkms "

# VPN
PROGRAMS+="wireguard-tools networkmanager-openvpn "
PROGRAMS_AUR+="python-eduvpn-client "


################################################################################
# Installation and configuration
################################################################################

# Exit if any command fails
set -e

COUNTER=1
STEPS=22

# Update
echo "($COUNTER/$STEPS) Synchronizing package databases..."
sudo pacman -Syu
echo ""
let COUNTER++

# Install system programs
echo "($COUNTER/$STEPS) Installing system programs..."
sudo pacman -S --needed $PROGRAMS_SYSTEM
echo ""
let COUNTER++

# Configure fonts
echo "($COUNTER/$STEPS) Configuring fonts..."
sudo ln -sv /etc/fonts/conf.avail/10-sub-pixel-rgb.conf /etc/fonts/conf.d/
sudo ln -sv /etc/fonts/conf.avail/11-lcd-filter-default.conf /etc/fonts/conf.d/
sudo ln -sv /etc/fonts/conf.avail/70-no-bitmaps.conf /etc/fonts/conf.d/
echo ""

# Configure SSH
echo "($COUNTER/$STEPS) Copying SSH keys..."
if [ ! -d $HOME/.ssh ]; then
    mkdir -v $HOME/.ssh
fi
install -o $USER -m 400 --verbose $SSH_KEYS_DIR/* $HOME/.ssh/
echo ""
let COUNTER++

# Install custom scripts
echo "($COUNTER/$STEPS) Copying SSH keys..."
if [ ! -d $HOME/Projects/ ]; then
    mkdir -v $HOME/Projects/
fi
git clone git@gitlab.com:felix.strk/scripts.git $HOME/Projects
CURRENT_PWD=$PWD
cd $HOME/Projects/scripts
/bin/sh $PWD/install.sh
cd $CURRENT_PWD
echo ""
let COUNTER++

# Install desktop environment
echo "($COUNTER/$STEPS) Installing graphical user interface..."
echo "    Display server protocol:  wayland"
echo "    Window manager:           sway"
echo "    Multimedia framework:     pipewire"
echo ""
sudo pacman -S --needed $PROGRAMS_DE
yay -S $PROGRAMS_DE_AUR
echo ""
let COUNTER++

# Enable pipewire
echo "($COUNTER/$STEPS) Enabling pipewire..."
systemctl --user enable pipewire pipewire-pulse wireplumber
echo ""
let COUNTER++

# Enable bluetooth
echo "($COUNTER/$STEPS) Enabling bluetooth..."
sudo usermod -aG lp $USER
sudo systemctl enable bluetooth
echo ""

# Enable dunst
echo "($COUNTER/$STEPS) Enabling dunst..."
systemctl --user enable dunst
echo ""
let COUNTER++

# Enable gammastep
echo "($COUNTER/$STEPS) Enabling gammastep..."
systemctl --user enable gammastep
echo ""
let COUNTER++

# Enable sway session target
echo "($COUNTER/$STEPS) Creating sway session target..."
cp -v "$PWD/config/systemd/sway-session.target" "$HOME/.config/systemd/user/sway-session.target"

# Enable kanshi
echo "($COUNTER/$STEPS) Enabling kanshi..."
cp -v "$PWD/config/systemd/kanshi.service" "$HOME/.config/systemd/user/kanshi.service" &&
systemctl --user enable kanshi.service
echo ""
let COUNTER++

# Set low battery policy
echo "($COUNTER/$STEPS) Setting low battery policy..."
cp -v "$PWD/config/systemd/battery-notification.service" "$HOME/.config/systemd/user/battery-notification.service" &&
systemctl --user enable battery-notification.service
sudo cp "$PWD/config/udev/99-lowbat.rules" "/etc/udev/rules.d/"
echo ""
let COUNTER++

# Configure logging
echo "($COUNTER/$STEPS) Configuring logging..."
sudo systemctl enable logrotate.timer
echo ""
let COUNTER++

# Configure logging for sway
echo "($COUNTER/$STEPS) Configuring logging for sway..."
sudo mkdir /var/log/sway
sudo touch /var/log/sway.log
sudo chown "$USER":"$USER" /var/log/sway
sudo chown "$USER":"$USER" /var/log/sway/sway.log
sudo chmod 755 /var/log/sway
sudo chmod 644 /var/log/sway/sway.log
sudo install -o root -m 644 --verbose ./.config/logrotate/sway /etc/logrotate.d/
echo ""
let COUNTER++

# Install desktop programs
echo "($COUNTER/$STEPS) Installing desktop programs..."
sudo pacman -S --needed $PROGRAMS
yay -S $PROGRAMS_AUR
echo ""
let COUNTER++

# Configure programs
echo "($COUNTER/$STEPS) Configuring programs..."
git clone git@gitlab.com:felix.strk/dotfiles.git $HOME/Projects
CURRENT_PWD=$PWD
cd $HOME/Projects/dotfiles
/bin/sh $PWD/install.sh
cd $CURRENT_PWD
echo ""
let COUNTER++

# Configure CUPS
echo "($COUNTER/$STEPS) Configuring CUPS..."
echo "($COUNTER/$STEPS) Updating PPD-Files..."
cups-genppdupdate
echo "($COUNTER/$STEPS) Enabling CUPS"
sudo systemctl enable cups
echo ""
let COUNTER++

# Configure theme
echo "($COUNTER/$STEPS) Configuring themes..."
gsettings set org.gnome.desktop.interface gtk-theme 'Adwaita-dark'
gsettings set org.gnome.desktop.interface font-name 'Cantarell 10'
gsettings set org.gnome.desktop.interface icon-theme 'breeze-dark'
echo ""
let COUNTER++

# Configure Keepass
echo "($COUNTER/$STEPS) Copying keepass keyfile..."
if [ ! -d $HOME/.local/share/keepass/ ]; then
    mkdir -pv $HOME/.local/share/keepass/
fi
install -o $USER -m 400 --verbose $KEEPASS_KEY_FILE $HOME/.local/share/keepass/
echo ""
let COUNTER++

# Configure kubectl
echo "($COUNTER/$STEPS) Copying kubectl config..."
if [ ! -d $HOME/.kube/ ]; then
    mkdir -pv $HOME/.kube/
fi
install -o $USER -m 400 --verbose $KUBECTL_FILE $HOME/.kube/
echo ""
let COUNTER++

# Configure Wireguard
echo "($COUNTER/$STEPS) Configuring wireguard for a VPN connection to jarvis..."
sudo install -o root -m 400 --verbose $WG_CONFIG_FILE /etc/wireguard/
echo ""
let COUNTER++

echo ""
echo "Configuration finished!"
echo "Please reboot your system!"
