#!/bin/sh

# Requires: yay

# Support for Brother DCP-585CW and MFC-J4420DW
echo "Installing support for Brother DCP-585CW and MFC-J4420DW"
yay -S brother-dcp585cw brother-mfc-j4420dw
echo ""

# Remove margin bug by DCP-585CW
echo "Removing margin bug..."
sudo sed -i 's/Letter/A4/g' /usr/share/brother/Printer/dcp585cw/inf/brdcp585cwrc
sudo systemctl restart cups
echo ""

echo "Finished! Add your new printers now!"
echo "You can either register your new printer using the web interface at:"
echo "  http://localhost:631/"
echo "or using the application Printer Settings"
