#!/bin/sh

HOSTNAME="ironman"
REGION="Europe"
City="Berlin"

CONFIG_DIR="./config"
SCRIPTS_DIR="./scripts"

# Exit if any command fails
set -e

# This script must run with root priviliges.
if [ $EUID -ne 0 ]; then
   echo "error: you cannot perform this operation unless you are root." 
   exit 1
fi

COUNTER=1
STEPS=9

# Set the time zone
echo "($COUNTER/$STEPS) Setting the time zone..."
echo "   Region: $REGION"
echo "   City: $CITY"
ln -sfv "/usr/share/zoneinfo/$REGION/$CITY" /etc/localtime
echo "($COUNTER/$STEPS) Synchronize hardware clock and system clock..."
hwclock --systohc
echo ""
let COUNTER++

# Localization
echo "($COUNTER/$STEPS) Generating locales..."
sed -i 's/#en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/g' /etc/locale.gen
sed -i 's/#de_DE.UTF-8 UTF-8/de_DE.UTF-8 UTF-8/g' /etc/locale.gen
locale-gen
install -m 644 $CONFIG_DIR/locale.conf /etc
echo ""
let COUNTER++

# Keyboard layout
echo "($COUNTER/$STEPS) Setting keyboard layout..."
cat > /etc/vconsole.conf <<EOF
# /etc/vconsole.conf
#
# persistent keyboard layout in tty

KEYMAP=de-latin1-nodeadkeys
EOF
echo ""
let COUNTER++

# Update mirrorlist
echo "($COUNTER/$STEPS) Updating mirrorlist..."
pacman -S --needed pacman-contrib
git clone https://gitlab.com/FStrk/scripts.git /tmp/scripts/
bash /tmp/scripts/scripts/update-mirrorlist
rm -R /tmp/scripts/
echo ""
let COUNTER++

# Set password for root
echo "($COUNTER/$STEPS) Adding a password for the root user..."
passwd
echo ""
let COUNTER++

# Network configuration
echo "($COUNTER/$STEPS) Configuring network..."
echo $HOSTNAME > /etc/hostname
echo "" >> /etc/hosts
echo "127.0.0.1    localhost" >> /etc/hosts
echo "::1          localhost" >> /etc/hosts
echo "127.0.1.1    $HOSTNAME.localdomain    $HOSTNAME" >> /etc/hosts
echo ""
let COUNTER++

# Install microcode
echo "($COUNTER/$STEPS) Installing microcode..."
pacman -S --needed efivar intel-ucode
echo ""
let COUNTER++

# Install GRUB
echo "($COUNTER/$STEPS) Installing GRUB..."
pacman -S --needed grub efibootmgr os-prober
echo ""
grub-install --target=x86_64-efi --efi-directory=/efi --bootloader-id=GRUB
echo ""
let COUNTER++

# Configure GRUB
echo "($COUNTER/$STEPS) Configuring GRUB..."
cp /etc/default/grub /etc/default/grub.bak
install -o root -m 644 $CONFIG_DIR/grub /etc/default
grub-mkconfig -o /boot/grub/grub.cfg
echo ""
let COUNTER++


echo "Please exit the chroot environment and unmount the devices with"
echo "    umount -R /mnt"
echo "and reboot your system!"
