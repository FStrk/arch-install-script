#!/bin/sh

USERNAME="felix"

# Exit if any command fails
set -e

# This script must run with root priviliges.
if [ $EUID -ne 0 ]; then
   echo "error: you cannot perform this operation unless you are root." 
   exit 1
fi

COUNTER=1
STEPS=7

# Enable NetworkManager
echo "($COUNTER/$STEPS) Enabling NetworkManager..."
systemctl enable NetworkManager
systemctl start NetworkManager
echo ""
let COUNTER++

# Adjust time server
echo "($COUNTER/$STEPS) Adjusting Time server..."
sed -i '/^#NTP=/c\NTP=ptbtime1.ptb.de ptbtime2.ptb.de ptbtime3.ptb.de' /etc/systemd/timesyncd.conf
sed -i '/^#FallbackNTP=/c\FallbackNTP=0.de.pool.ntp.org 0.arch.pool.ntp.org' /etc/systemd/timesyncd.conf
timedatectl set-ntp true
timedatectl status
timedatectl timesync-status
echo ""
let COUNTER++

# Create a new sudo user
echo "($COUNTER/$STEPS) Creating a new user with sudo priviliges..."
useradd --create-home --shell /bin/zsh --groups wheel,video $USERNAME
passwd $USERNAME
echo "Please edit the sudoers file appropriately "
EDITOR=nano visudo
echo ""
let COUNTER++

# Configure makepkg
echo "($COUNTER/$STEPS) Configuring makepkg..."
sed -i '/MAKEFLAGS=/c\MAKEFLAGS=\"-j\$(nproc)\"' /etc/makepkg.conf
echo ""
let COUNTER++

# Configure fstrim
echo "($COUNTER/$STEPS) Configuring fstrim..."
systemctl enable fstrim.timer fstrim.service
systemctl start fstrim.timer fstrim.service
systemctl status fstrim.timer fstrim.service
echo ""
let COUNTER++

# Configure firewall
echo "($COUNTER/$STEPS) Installing firewall..."
sudo pacman -S --needed ufw
echo ""
echo "($COUNTER/$STEPS) Configuring firewall..."
systemctl enable ufw
ufw default deny
ufw enable
ufw status verbose
echo ""
let COUNTER++

# Dual boot time issue
echo "($COUNTER/$STEPS) Solve time issue caused by dual boot..."
timedatectl set-local-rtc 1 --adjust-system-clock
echo ""
let COUNTER++

echo "Please reboot your system and login as $USERNAME."
