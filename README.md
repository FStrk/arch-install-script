# Arch linux installation

This repo contains scripts to install and configure Arch linux for my personal use.
In particular, the configuration heavily relies on my personal setup.


## 1. Pre-installation

* Follow the steps from section 1 of the installation guide in the [Arch wiki](https://wiki.archlinux.org/title/installation_guide).

## 2. Installation

* Install the base system.
    ```pacstrap base linux linux-firmare base-devel linux-headers linux-lts linux-lts-headers nano networkmanager git zsh```

* Generate an fstab file.
  ```genfstab -U /mnt >> /mnt/etc/fstab```

* Chroot into the new system.
  ```arch-chroot /mnt```

* Execute the script `./scripts/2-first-steps.sh`.

  * Required:
    * `./config/locale.conf`
    * `./config/grub`
    * `./scripts/update-mirrorlist`

  * Adjustable variables
    | Variable      | Default   | 
    |:--------------|:----------|
    | HOSTNAME      | ironman   |
    | REGION        | Europe    |
    | CITY          | Berlin    |


## 3. Post installation configuration

* Execute the script `./scripts/3-root.config.sh`.

  * Adjustable variables:
    | Variable      | Default   | 
    |:--------------|:----------|
    | USERNAME      | felix     |

## 4. Install yay to use the AUR

* Execute the script `./scripts/4-install-yay.sh`

## 5. Install and configure the graphical user interface

* Execute the script `./scripts/5-install-sway.sh`
  
  * Required:
    * `./config/systemd/battery-notification.service`
    * `./config/systemd/kanshi.service`
    * `./config/systemd/sway-session.target`
    * `./config/udev/99-lowbat.rules`

  * Adjustable variables
    | Variable         | Description   | 
    |:-----------------|:--------------|
    | CONFIG_DIR       | Local directory to store configuration files |
    | SSH_KEYS_DIR     | Directory of the SSH keys to copy |
    | KEEPASS_KEY_FILE | Location of the key file of my keepass database |
    | KUBECTL_FILE     | Location of my kubectl config file |
    | WG_CONFIG_FILE   | Location of my wireguard config file | 

* To unlock keyring by login follow the instruction at https://wiki.archlinux.org/title/GNOME/Keyring#Using_the_keyring

* Configure personal printers and scanners by executing
  * `./scripts/config-printer.sh`
  * `./scripts/config-scanner.sh`
